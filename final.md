# Sujet 1 : Pour un numérique soutenable, « des bonnes intentions [...] ce n'est pas suffisant »

La soutenabilité écologique du numérique est un sujet de plus en plus important de l'anthropocène. Outil emblématique des XX et XXIeme siècles, le numérique procèderait d'une révolution anthropologique, faisant du calculable le nouveau critère de penser de l'être-humain. Tout comme les mécaniciste du XVIIIeme siècle considéraient l'automate comme modèle thaumaturge, nous pretons aujourd'hui au numérique notre salvation face au désastre écologique avancé par la collapsologie.
Cette idée se retrouve dans l'extrait d'un article de Sébastion Gavois écrit dans NextInpact en janvier 2021 mettant en avant la position de l'Arcep pour concilier numérique et écologie.
En effet, dans l'extrait, cette autorité administrative indépendante, chargée d'assurer la régulation des secteurs des communications électroniques, des postes et de la distribution de la presse, estime que le numérique aurait son rôle à jouer au sein d'stratégie bas carbone, visant à limiter l'impact humaine sur l'environnement. Pour l'Arcep, le rôle du numérique est même essentiel face à ces enjeux car il permettrait d'optimiser tout un ensemble de secteurs. Après avoir présenté les avantages du numérique dans une stratégie tendant vers la sobriété carbonée, nous opposerons aux arguments avancés des limites inhérentes à l'outil.

Le numérique pourrait caractérise un aboutissement de la raison computationnelle. En effet, cet outil marque l'avènement du tout calculable, incarné par l'information.
Dans ce paradigme, le monde se présente est une potentielle donnée mesurable, manipulable et stockable dans des disques durs locaux voire à distance, dans le cloud. Les immenses volumes de data obtenus peuvent être analysés et traités par des puissances de calcul toujours plus élevées, et ainsi nous offrir à penser des indicateurs toujours plus précis, en mesure d'influencer les décisions importantes des acteurs privés, publics voire des consommateurs, à venir en termes de baisse du carbone.
De plus, par une surveillance globale de l'état des ressources de notre environnement, le numérique pourrait constituer un panoptique de l'écologie. En nous offrant la possibilité de surveiller toute donnée pouvant l'être, celui-ci nous permettrait d'améliorer le contrôle de nos impacts par le biais de la technique et des savoirs qu'elle produit.
Ce phénomène peut se concrétiser dans la confiance que nous attribuons aux projets de *Green IT*, d'*Internet Of Things* voire de *Smart Cities* bardées de capteurs derniers cris, afin de limiter la casse au travers les indicateurs qu'ils nous offriraient.

Pour résumer la pensée de l'Arcep quant à la place du numérique dans la transition vers le bas carbone, nous pourrions paraphraser Francis Bacon -- *Knowledge is power* - de la manière suivante : le numérique, par le savoir et les indicateurs qu'il nous offriraient, permettrait d'optimiser notre processus de décision.

Or, si l'idée d'une surveillance salvatrice est toute à fait entendable, que malgré son rôle, le numérique ne soit pas exempt de faire des progrès vers la sobriété, il existe de profondes limites à la stratégie de l'Arcep.

En effet, si le numérique nous offre des indicateurs calculés, discrétisés, ceux-ci restent profondément incomplets, non représentatifs de la situation réelle. Oui le numérique peut permettre, tout comme le nucléaire, une stratégie bas carbone. Non, cette stratégie n'est pas suffisante, car par exemple, les necessaires extractions permettant de construire le physique du numérique sont elles aussi extrêmement polluantes. L'abstraction implicite n'est pas fidèle au réel, une carte n'est jamais l'égal d'un territoire. En ne centrant notre jugement que sur les indicateurs offerts par le numérique, nous pourrions passer à coter ces conséquences concrète de ce dernier, et les efforts consentis pour aller vers une stratégie bas carbone, qu'on peut espérer lier à une préoccupation écologiques, ne serviraient à rien.

Ainsi, si la position de l'Arcep est louable, le numérique pouvant potentiellement aider à prolonger un peu plus longtemps les effets de l'anthropocene sur le système Terre, celle-ci témoigne des limites d'une écologie gestionnaire basée sur des indicateurs quantifiables qui, si on leur attribue aveuglement la réponse à nos maux, risquent de nous faire passer à coter des réalités profondes des questions écologiques.

## Source
- Pour un numérique soutenable, « des bonnes intentions [...] ce n'est pas suffisant », par Sébastien Gavois, in NextInpact, 06/01/2021
- Définition de l'Arcep : https://www.arcep.fr/larcep.html ;

Pierre La Rocca
Final sous licence libre CC-by : permet de diffuser le contenu sous condition d'attribution.
